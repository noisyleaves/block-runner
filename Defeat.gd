extends Area

func defeat(body):
	if body == GlobalConstants.Player:
		GlobalConstants.CameraNode.reset()
		GlobalConstants.Player.reset()
		
		get_tree().get_root().get_node("Spatial").reset()
