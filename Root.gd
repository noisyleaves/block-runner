extends Spatial

var floorPiece = load("res://FloorPiece.tscn")
var startScale
const floorDistantApart = -30

func _ready():
	GlobalConstants.Player = $Player
	GlobalConstants.CameraNode = $Camera
	startScale = $Start.get_scale()
	randomize()
	
	reset()

func add_floor_piece(i):
	var floorPieceInstance = floorPiece.instance()
	var scale = randf() + 0.1
	var xPos = randf() * 20 - 10
	
	floorPieceInstance.set_scale(Vector3(startScale.x * scale, startScale.y, startScale.z * scale))
	floorPieceInstance.translation.z = floorDistantApart * i
	floorPieceInstance.translation.x = xPos
	
	$FloorPieces.add_child(floorPieceInstance)

func reset():
	for n in $FloorPieces.get_children():
		$FloorPieces.remove_child(n)
		n.queue_free()
	
	for i in range(100):
		add_floor_piece(i + 1)
