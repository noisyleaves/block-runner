extends KinematicBody
var targetPos
var racastingHelper = load("res://Raycasting.gd").new()
var velocity = Vector3.ZERO

const MAX_SPEED = 1500
const SPEED_MULTIPLIER = 1500
const MOVEMENT_THRESHOLD = 1
const GRAVITY = 80
const JUMP_SPEED = 50

func _physics_process(delta):    
	if Input.is_mouse_button_pressed(1):
		targetPos = racastingHelper.get_click_pos_on_screen_at_y_0(get_parent().get_node("Camera"), get_viewport().get_mouse_position())
	
	velocity.y -= GRAVITY * delta
	
	if targetPos != null:
		velocity.x = velocity_to_target(targetPos.x, translation.x) * delta
		velocity.z = velocity_to_target(targetPos.z, translation.z) * delta
	
	velocity = move_and_slide(velocity , Vector3.UP)

func velocity_to_target(targetPos, translation):
	if abs(targetPos - translation) > MOVEMENT_THRESHOLD:
		var multiplier = 1
			
		if targetPos < translation:
			multiplier = -1

		var desiredVelocity = min(MAX_SPEED,  abs((targetPos - translation)) * SPEED_MULTIPLIER)

		return multiplier * desiredVelocity
	else:
		return 0

func jump():
	if velocity.y <= MOVEMENT_THRESHOLD:
		velocity.y = JUMP_SPEED
		$Particles.restart()

func reset():
	targetPos = null
	velocity = Vector3.ZERO
	translation = Vector3(0,1,0)
