extends Camera

var initialTranslation
var initialRotation
var ticksSinceStarted = 0

const MAX_DISTANCE_BEHIND_PLAYER = 30
const CAMERA_SPEED_MULTIPLIER = 4

func _ready():
	initialTranslation = translation
	initialRotation = rotation_degrees

func _process(delta):
	ticksSinceStarted += delta
	translation.z -= delta * pow(ticksSinceStarted, 0.5)
	
	translation.z = min(translation.z, GlobalConstants.Player.translation.z + MAX_DISTANCE_BEHIND_PLAYER)

func reset():
	translation = initialTranslation
	rotation_degrees = initialRotation
	ticksSinceStarted = 0
