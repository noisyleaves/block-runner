extends Node
const ray_length = 99999

func get_click_pos_on_screen_at_y_0(camera, mousePosition):
	var a = get_viewport() 
	var from = camera.project_ray_origin(mousePosition) 
	var to = from + camera.project_ray_normal(mousePosition) * ray_length

	var lengthAlongLine = abs((from.y) / get_line_length(from.y, to.y))
	var x = from.x - lengthAlongLine * get_line_length(from.x, to.x)
	var z = from.z - lengthAlongLine * get_line_length(from.z, to.z)
	
	return Vector3(x, 0, z)

func get_line_length(start, end):
	return start - end
